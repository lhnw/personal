import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;
import org.jsoup.Jsoup;
import java.util.Enumeration;
import org.jsoup.nodes.Document;
import java.io.IOException;
public class Score {
	public static void main(String[] args) throws IOException{	
			//初始化经验值
			double self_before=0;
			double self_base=0;	
			double self_test=0;
			double self_program=0;
			double self_add=0;
			// 读取配置文件
			Properties p= new Properties();
			p.load(new FileInputStream("peizhi/total.properties"));
			Enumeration fileName = p.propertyNames();
			// 把总分赋值
			double  total_before = Integer.parseInt(p.getProperty("before"));
			double  total_base = Integer.parseInt(p.getProperty("base"));
			double  total_test = Integer.parseInt(p.getProperty("test"));
			double  total_program = Integer.parseInt(p.getProperty("program"));
			double  total_add = Integer.parseInt(p.getProperty("add"));
			//读取两个HTML信息
			try {
			File f1 = new File("small.html");
			File f2 = new File("all.html");
			Document d1= Jsoup.parse(f1, "UTF-8");
			Document d2 = Jsoup.parse(f2, "UTF-8");
			//解析小班课经验值并相加
			int leng1=d1.select("div[class=interaction-row]").size();
			for (int i=0;i<leng1;i++) {
				int leng2=d1.select("div[class=interaction-row]").get(i).select("span").size();
				//利用小标题名称对同类题型进行分数累加
				String row=d1.select("div[class=interaction-row]").get(i).select("span").get(1).text();
				int leng3=d1.select("div[class=interaction-row]").get(i).select("span").size();
				if(row.indexOf("自测") != -1) 
				{
					String selfget=d1.select("div[class=interaction-row]").get(i).select("span").get(leng3-1).text();
					self_before=self_before+isNum(selfget);
				}
				else if(row.indexOf("小测") != -1) 
				{
					String testget=d1.select("div[class=interaction-row]").get(i).select("span").get(leng3-1).text();
					self_test=self_test+isNum(testget);
				}
				else if(row.indexOf("课堂完成") != -1)
				{
					String baseget=d1.select("div[class=interaction-row]").get(i).select("span").get(leng3-1).text();
					self_base=self_base+(isNum(baseget));
				}
				else if(row.indexOf("编程") != -1)
				{
					String proget=d1.select("div[class=interaction-row]").get(i).select("span").get(leng3-1).text();
					self_program=self_program+(isNum(proget));
					
				}
				else if(row.indexOf("附加") != -1) 
				{
					String addget=d1.select("div[class=interaction-row]").get(i).select("span").get(leng3-1).text();
					self_add=self_add+(isNum(addget));
				}
			}	
			//解析大班课经验值
			int leng11 =  d2.select("div[class=interaction-row]").size();
			for (int i = 0;i<leng11;i++) 
			{
				int leng22=d2.select("div[class=interaction-row]").get(i).select("span").size();					
				String rowsa=d2.select("div[class=interaction-row]").get(i).select("span").get(1).text();
				int leng33=d2.select("div[class=interaction-row]").get(i).select("span").size();
				if(rowsa.indexOf("自测")!=-1) 
				{
					String selfget=d2.select("div[class=interaction-row]").get(i).select("span").get(leng33-1).text();
					self_before+=isNum(selfget);
				}
			}
			//把各类成绩换算成百分制
			double before=self_before/(double) total_before* 100;
			double base=self_base/(float) total_base* 100*0.95;
			double test=self_test/(double) total_test* 100;
			double program=self_program/ total_program* 100;
			 //如果成绩大于95分，按95分计算
            if(program>95)
				program=95;
			double add=self_add/ total_add* 100;
            //如果成绩大于90分，按90分计算
			if(add>90)
				add=90;
			//计算最终成绩
			double finalresult = before*0.25+base*0.3+test*0.2+program*0.1+add*0.05;
			String mark= String .format("%.2f",finalresult);
			System.out.println(mark);}
			catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
        
	//将得到的经验值转化为double类型
	public static int isNum(String str)
	{
		String s = "";
		for(int i = 0;i<str.length();i++)
		{
			char chr = str.charAt(i);
			if(chr  >= 48 && chr <= 57)
				 s = s + chr;
		}
		int a = Integer.parseInt(s);
		return a;
	}
}
